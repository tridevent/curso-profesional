﻿using UnityEngine;
using System.Collections;

public class C_angularVelocity : MonoBehaviour {
    Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.angularVelocity = new Vector3(7, 0, 0);
	}
}
