﻿using UnityEngine;
using System.Collections;

public class C_angularDrag : MonoBehaviour {
    Rigidbody rb;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        rb.angularDrag = 35;
	}
}
