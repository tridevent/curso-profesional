﻿using UnityEngine;
using System.Collections;

public class C_velocity : MonoBehaviour {
    Rigidbody rb;
     public float speed;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical") )* Time.deltaTime * speed;
	}
}
