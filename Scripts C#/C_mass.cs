﻿using UnityEngine;
using System.Collections;

public class C_mass : MonoBehaviour {
    Rigidbody rb;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        rb.mass = 10;
	}
}
