﻿using UnityEngine;
using System.Collections;

public class C_freezeRotation : MonoBehaviour {
    Rigidbody rb;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
