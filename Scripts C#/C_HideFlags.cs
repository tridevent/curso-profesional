﻿using UnityEngine;
using System.Collections;

public class C_HideFlags : MonoBehaviour {
	public GameObject objeto;
	// Use this for initialization
	void Start () {
		//objeto.hideFlags = HideFlags.HideInInspector;
		//objeto.hideFlags = HideFlags.HideInHierarchy;
		objeto.hideFlags = HideFlags.NotEditable;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
